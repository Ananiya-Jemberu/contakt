import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.widget.Toast
import androidx.annotation.Nullable
import com.ultraone.contakt.messaging.MessagingActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.system.measureTimeMillis

class Services : Service() {
    private val mBinder = LocalBinder()
    var handler: Handler? = null
    var mToast: Toast? = null

    inner class LocalBinder : Binder() {
        val service: Services
            get() = this@Services
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    override fun onCreate() {
        super.onCreate()
        CoroutineScope(Dispatchers.IO).launch {
            measureTimeMillis {
                withContext(Dispatchers.Default) {

                }
            }
        }

    }

    override fun onDestroy() {

        CoroutineScope(Dispatchers.IO).launch {
            measureTimeMillis {
                withContext(Dispatchers.Default) {

                }
            }
        }
            super.onDestroy()
    }

    override fun onStartCommand(@Nullable intent: Intent, flags: Int, startId: Int): Int {
        handler = Handler()
        handler!!.post {
            CoroutineScope(Dispatchers.IO).launch{
                measureTimeMillis {
                    withContext(Dispatchers.Default) {
                    }
                }
            }
        }
        return START_STICKY
    }
}