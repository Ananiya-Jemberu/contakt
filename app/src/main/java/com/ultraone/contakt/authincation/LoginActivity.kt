package com.ultraone.contakt.authincation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.ultraone.contakt.R
import com.ultraone.contakt.messaging.MessagingActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if(FirebaseAuth.getInstance().uid != null){
            val i = Intent(this, MessagingActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        }else{
            val i = Intent(this, SignUpActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        }
        textView.setOnClickListener {
            val intent = Intent(this@LoginActivity, SignUpActivity::class.java)
            startActivity(intent)

        }
        button2.setOnClickListener{
            if(editTexte.text.isEmpty() || editTextp.text.isEmpty()){

                Snackbar.make(c,"please sign in with correct word",Snackbar.LENGTH_LONG).show()
            }else{
                FirebaseAuth.getInstance().signInWithEmailAndPassword(editTexte.text.toString(),editTextp.text.toString())
                    .addOnSuccessListener {
                        val intent = Intent(this, MessagingActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }
            }
        }

    }
}
