package com.ultraone.contakt.authincation

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Window

import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.ultraone.contakt.messaging.MessagingActivity
import com.ultraone.contakt.R
import com.ultraone.contakt.data.AllUsers
import kotlinx.android.synthetic.main.activity_image_entering.*
import kotlinx.android.synthetic.main.activity_image_entering.view.*


import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.view.*
import kotlinx.android.synthetic.main.error_dialog.*
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.*

class SignUpActivity : AppCompatActivity() {
    var uri: Uri?  = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main) ;
        if(FirebaseAuth.getInstance().uid != null) {
            val i = Intent(this, MessagingActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or (Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        }
        textView.setOnClickListener{
            startActivity(Intent(this@SignUpActivity,LoginActivity::class.java))
        }
        signUp()

    }  @SuppressLint("SetTextI18n")
    private fun signUp(){
        //next button on click
        button2.setOnClickListener {
            val email = editText3.text.toString()
            val password = editText.text.toString()
            if(email.isEmpty() || password.isEmpty()){
                Snackbar.make(one,"pleas put something", Snackbar.LENGTH_LONG).show()
            }else if (email.isNotEmpty() || password.isNotEmpty()){
               //setContentView(R.layout.activity_image_entering)
                val i =  layoutInflater.inflate(R.layout.activity_image_entering, one,false)
                var res: Int? = null

                i.next.text = "SKIP"
                i.men_w.setOnClickListener {
                    i.next.text = "NEXT"
                    rbutton.alpha = 0F
                    res = R.drawable.men_w
                    i.button.setImageResource(R.drawable.men_w)
                    //i.button.setBackgroundResource(R.drawable.men_w)
                }
                i.men_b.setOnClickListener {
                    res = R.drawable.men_b
                    i.next.text = "NEXT"
                    rbutton.alpha = 0F
                    i.button.setImageResource(R.drawable.men_b)
                }
                i.women_w.setOnClickListener {
                    res = R.drawable.women_w
                    i.next.text = "NEXT"
                    rbutton.alpha = 0F
                    i.button.setImageResource(R.drawable.women_w)
                }
                i.women_b.setOnClickListener {
                    res = R.drawable.women_b
                    i.next.text = "NEXT"
                    rbutton.alpha = 0F
                    //val bd: Drawable = BitmapDrawable(,R.drawable.women_b)
                    //i.
                    i.button.setImageResource(R.drawable.women_b)
                }
                fun start(){
                    val intent = Intent(Intent.ACTION_PICK)
                    intent.type = "image/*"
                    startActivityForResult(intent, 0)
                }
                i.button.setOnClickListener {start()}

                i.rbutton.setOnClickListener {start()}
                i.next.setOnClickListener{
                    emailAndPassword(email,password,res)
                }

                one.addView(i)
                //al inflate = layoutInflater.inflate(R.layout.activity_image_entering,one)
                //one.removeView(containerr)
                //one.addView(inflate)
                // putting the picture
                //inflate.button.setOnClickListener{
                //val intent =  Intent(Intent.ACTION_PICK)
                //intent.type = "image/*"
                //startActivityForResult(intent,0)

                //}

                // dialog next button on click
                //inflate.next.setOnClickListener {
                //creatingEmailAndPassword(email,password)
                //}*/
            }
        }
    }
    // uploading the profile
    fun emailAndPassword(email : String, password : String, res: Int?) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if(!it.isSuccessful) return@addOnCompleteListener
                Snackbar.make(image_entering,"sign up complete ${it.result?.user?.uid}", Snackbar.LENGTH_LONG).show()
                uploadImage(res)

            }
            .addOnFailureListener {
                errorDialog(it.message.toString())
            }
    }
    // uploading image to fire base
    private fun uploadImage(res: Int?) {
        val bmp = res?.let { BitmapFactory.decodeResource(resources, it) }
        val baos = ByteArrayOutputStream()


        if(res != null) {
            bmp?.compress(Bitmap.CompressFormat.PNG,100,baos)
            val data = baos.toByteArray()
            val filename = UUID.randomUUID().toString()
            val rf = FirebaseStorage.getInstance().getReference("images/$filename")
            rf.putBytes(data)
                .addOnSuccessListener {

                    image_entering.let { it1 -> Snackbar.make(it1, "bitmap upload success" , Snackbar.LENGTH_SHORT).show() }
                    rf.downloadUrl.addOnSuccessListener {
                        saveUserProfileToDataBase(it.toString())
                    }
                }
                .addOnFailureListener{
                    errorDialog(it.message.toString())
                }
        }
        if(uri == null) return
        val filename = UUID.randomUUID().toString()
        val rf = FirebaseStorage.getInstance().getReference("images/$filename")
        rf.putFile(uri!!)
            .addOnSuccessListener {

                image_entering.let { it1 -> Snackbar.make(it1, "upload success" , Snackbar.LENGTH_SHORT).show() }
                rf.downloadUrl.addOnSuccessListener {
                    saveUserProfileToDataBase(it.toString())
                }
            }
            .addOnFailureListener{
                errorDialog(it.message.toString())
            }
    }
    //error dialog
    fun errorDialog(error: String) {
        val dialog = Dialog(this)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.error_dialog)
        dialog.error_text.text = error
        dialog.setCancelable(false)

        dialog.ok.setOnClickListener{}
        dialog.cancel.setOnClickListener{dialog.dismiss()}
        dialog.show()

    }
    // parsing and saving the image
    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){

            //val mview = layoutInflater.inflate(R.layout.activity_image_entering,one,false)
            Log.d("onActivityResult","works")
            val email = editText3.text.toString()
            val password = editText.text.toString()
            Log.d("onActivityResult", "request was successful")
            uri = data.data
            val bmp = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            val bd: Drawable = BitmapDrawable(bmp)
            rbutton.alpha = 0F
            Log.d("onActivityResult","btn clicked")
            button.setImageBitmap(bmp)
           // one.addView(mview)
            next.text = "NEXT"
           next.setOnClickListener {

                if(email.isNotEmpty() || password.isNotEmpty()){
                    emailAndPassword(email,password,null)
                } else {
                    image_entering.let { it1 ->
                        Snackbar.make(it1,"pleas put something in the email and password field" ,
                            Snackbar.LENGTH_SHORT)
                            .show()
                    }

                }

            }
        }
    }
    // save user to database
    private fun saveUserProfileToDataBase(profileImageUrl: String){
        val name = editText4.text.toString()

        val fi = FirebaseDatabase.getInstance()
        val fai = FirebaseAuth.getInstance()
        val uid = fai.uid ?: ""
        val rf = fi.getReference("/users/$uid")
        val users = AllUsers(name,profileImageUrl,uid)
        rf.setValue(users)
            .addOnSuccessListener {
                image_entering.let{Snackbar.make(it," i saves the profile to data base" , Snackbar.LENGTH_SHORT).show()}
                val intent = Intent(this, MessagingActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .addOnFailureListener{
                errorDialog(it.message.toString())
            }
    }

    /*override fun onRestart() {
        if(FirebaseAuth.getInstance().uid != null){
            val i = Intent(this, MessagingActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        }else{
            val i = Intent(this, SignUpActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        }
        super.onRestart()
    }*/
}

