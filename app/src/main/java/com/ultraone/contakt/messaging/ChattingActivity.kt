package com.ultraone.contakt.messaging

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.ultraone.contakt.R

import com.ultraone.contakt.data.AllUsers
import com.ultraone.contakt.data.Chat
import com.ultraone.contakt.messaging.adapters.ChattingFrom
import com.ultraone.contakt.messaging.adapters.ChattingTo
import com.ultraone.contakt.messaging.adapters.ReceiveFile
import com.ultraone.contakt.messaging.adapters.SendFile
import com.ultraone.contakt.messaging.ui.home.HomeFragment
//import com.ultraone.contakt.data.SendImage
import com.xwray.groupie.GroupAdapter

import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chatting.*
import java.text.SimpleDateFormat
import java.util.*


class ChattingActivity : AppCompatActivity() {

    var uri: Uri? = null
    var toUsers: AllUsers? = null
    private val adapter = GroupAdapter<ViewHolder>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_chatting)
        toUsers = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)
        actionBar?.title = toUsers?.username
        receive()

        chat_rv.adapter = adapter
        cv_image_button.setOnClickListener {

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
        //DaggerMagicBox.c
        chat_send.setOnClickListener {
            send()

        }


    }

    private fun sendImage() {
        if (uri == null) return
        val filename = UUID.randomUUID().toString()
        val rf = FirebaseStorage.getInstance().getReference("images/$filename")
        rf.putFile(uri!!)
            .addOnSuccessListener {

                chat_view.let { it1 ->
                    Snackbar.make(it1, "upload success", Snackbar.LENGTH_SHORT).show()
                }
                rf.downloadUrl.addOnSuccessListener {
                    sendImageToDataBase(it.toString())
                }
            }

    }

    @SuppressLint("SimpleDateFormat")
    private fun sendImageToDataBase(pUri: String) {
        val data = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)
        val text = chat_input.text.toString()
        val auth = FirebaseAuth.getInstance()
        val dataBase = FirebaseDatabase.getInstance()
        val from = auth.uid
        val to = data.uid

        if (from == null) return
        val sdf = SimpleDateFormat("hh:mm:ss")
        val currentDate = sdf.format(Date())
        val ref = dataBase.getReference("/user-to-user-file-sharing/$from/$to").push()
        val toRef = dataBase.getReference("/user-to-user-file-sharing/$to/$from").push()
        val send = Chat(ref.key!!, from, to, null, pUri, currentDate.toString())
        ref.setValue(send).addOnSuccessListener {

            chat_rv.scrollToPosition(adapter.itemCount - 1)
            Snackbar.make(chat_view, ref.key.toString(), Snackbar.LENGTH_LONG)
        }
        toRef.setValue(send).addOnCanceledListener {

        }
        val latestMref = FirebaseDatabase.getInstance().getReference("/latest-messages/$from/$to")
        latestMref.setValue(send).addOnSuccessListener {

        }
        val toLatestMref = FirebaseDatabase.getInstance().getReference("/latest-messages/$to/$from")
        toLatestMref.setValue(send)

    }

    private fun receive() {
        val data = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)
        val auth = FirebaseAuth.getInstance()
        val from = auth.uid
        val to = data.uid
        val dataBase = FirebaseDatabase.getInstance()
        val ref = dataBase.getReference("/user-to-user-file-sharing/$from/$to")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {

                val chatMessage = p0.getValue(Chat::class.java)




                //shows the message to sender
                if (chatMessage?.fromId == FirebaseAuth.getInstance().uid) {
                    if (chatMessage != null && chatMessage.imageUri != "") {
                        if (chatMessage.imageUri != null && chatMessage.imageUri != "") {
                            val cu = HomeFragment.currentUsers ?: return
                            adapter.add(SendFile(chatMessage, cu))
                        }
                    }
                    if (chatMessage != null && chatMessage.text != "") {

                        val cu = HomeFragment.currentUsers ?: return
                        adapter.add(ChattingTo(chatMessage, cu))
                        chat_rv.scrollToPosition(adapter.itemCount - 1)
                    }
                } else {
                //shows the message for receiver
                    if (chatMessage != null && chatMessage.text != "") {
                        toUsers = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)
                        if (toUsers != null) {
                            adapter.add(ChattingFrom(chatMessage, toUsers!!))
                            chat_rv.scrollToPosition(adapter.itemCount - 1)
                        }
                        // val data = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)

                    }
                    if (chatMessage != null && chatMessage.imageUri != "") {
                        toUsers = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)
                        if (toUsers != null) {


                            if (chatMessage.imageUri != null || chatMessage.imageUri != "") {

                                adapter.add(ReceiveFile(chatMessage, toUsers!!))
                                chat_rv.scrollToPosition(adapter.itemCount - 1)
                            }
                        }
                    }
                }


            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun send() {
        val data = intent.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)
        val text = chat_input.text.toString()
        val auth = FirebaseAuth.getInstance()
        val dataBase = FirebaseDatabase.getInstance()
        val from = auth.uid
        val to = data.uid

        if (from == null) return
        val ref = dataBase.getReference("/user-to-user-file-sharing/$from/$to").push()

        val toRef = dataBase.getReference("/user-to-user-file-sharing/$to/$from").push()
        if(text != "") {
            val sdf = SimpleDateFormat("hh:mm:ss")
            val currentDate = sdf.format(Date())

            val send = Chat(ref.key!!, from, to, text, null, currentDate.toString())
            ref.setValue(send).addOnSuccessListener {
                chat_input.text.clear()
                chat_rv.scrollToPosition(adapter.itemCount - 1)
                Snackbar.make(chat_view, ref.key.toString(), Snackbar.LENGTH_LONG)
            }
            toRef.setValue(send).addOnCanceledListener {
            }
            val latestMref =
                FirebaseDatabase.getInstance().getReference("/latest-messages/$from/$to")
            latestMref.setValue(send)
            val toLatestMref =
                FirebaseDatabase.getInstance().getReference("/latest-messages/$to/$from")
            toLatestMref.setValue(send)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            uri = data.data


            sendImage()
        }
    }

}


