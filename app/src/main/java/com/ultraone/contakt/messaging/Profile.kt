package com.ultraone.contakt.messaging

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.ultraone.contakt.R
import com.ultraone.contakt.data.AllUsers
import kotlinx.android.synthetic.main.profile.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlin.system.measureTimeMillis

class Profile: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)

        Circle_v.circleBackgroundColor = Color.CYAN
                    fetchProfile()


    }
    private fun fetchProfile() {


          FirebaseDatabase.getInstance().let {ref->
              ref.getReference("/users/${FirebaseAuth.getInstance().uid}").let {dbref->
                  dbref.addListenerForSingleValueEvent(object : ValueEventListener {
                      override fun onCancelled(p0: DatabaseError) {
                           //ODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                      }

                      override fun onDataChange(p0: DataSnapshot) {
                          val cu = p0.getValue(AllUsers::class.java)
                          if(cu != null) {
                              Picasso.get().load(cu.profileImageUrl)
                                  .into(Circle_v)  //TDO("not implemented") //To change body of created functions use File | Settings | File Templates.
                          }
                      }

                  })

              }

          }
    }

}