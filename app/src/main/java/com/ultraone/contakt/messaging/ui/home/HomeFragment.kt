package com.ultraone.contakt.messaging.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.UiThread
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import com.ultraone.contakt.R
import com.ultraone.contakt.data.AllUsers
import com.ultraone.contakt.data.Chat
import com.ultraone.contakt.messaging.ChattingActivity
import com.ultraone.contakt.messaging.CreatingMessage
import com.ultraone.contakt.messaging.Profile
import com.ultraone.contakt.messaging.adapters.MessageRows
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder

import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

class HomeFragment : Fragment() {

    var toUsers: AllUsers? = null
    companion object {
        var currentUsers: AllUsers? = null
    }


    val adapter = GroupAdapter<ViewHolder>()
    @UiThread
    suspend fun runOnBG(v: View){
        GlobalScope.launch {
            receiveLatestMessages(v)
        }
    }
    val messageMaps = HashMap<String, Chat>()
    private suspend fun receiveLatestMessages(v: View) = coroutineScope{
        val from = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference( "/latest-messages/$from")

        ref.addChildEventListener(object: ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                v?.shimmer.visibility = View.GONE
                v?.shimmer.stopShimmer()
                v?.ma_rv.visibility = View.VISIBLE
                val message = p0.getValue(Chat::class.java) ?: return
                if(message.text != ""){
                    toUsers = activity?.intent?.getParcelableExtra<AllUsers>(CreatingMessage.USER_KEY)

                    messageMaps[p0.key!!] = message
                    refresh()


                }

            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val message = p0.getValue(Chat::class.java) ?: return
                messageMaps[p0.key!!] = message
                refresh()

                adapter.add(MessageRows(message))

                val vib = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vib.vibrate(500)
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }

        })

    }

    private fun refresh() {
        adapter.clear()

        messageMaps.values.forEach {
            adapter.add(MessageRows(it))
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        root.ma_profile.setOnClickListener {
            Intent(activity, Profile::class.java).let{
                startActivity(it)
            }
        }
        root.ma_username.visibility = View.INVISIBLE
        root.ma_rv.adapter = adapter
        adapter.setOnItemClickListener { item, view ->
            Intent(activity, ChattingActivity::class.java).let{
                val row = item as MessageRows
                it.putExtra(CreatingMessage.USER_KEY, row.partnerUser)
                startActivity(it)
            }
        }
        CoroutineScope(Dispatchers.IO).launch{
            measureTimeMillis {
                withContext(Dispatchers.Default) {
                    runOnBG(root)
                }
            }
        }
        root.floatingActionButton.setOnClickListener{

            val i = Intent(activity, CreatingMessage::class.java)
            startActivity(i)
        }
        val uid = FirebaseAuth.getInstance().uid
        val rf = FirebaseDatabase.getInstance().getReference("/users/$uid")
        rf.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                currentUsers = p0.getValue(AllUsers::class.java)
                val users = p0.getValue(AllUsers::class.java)
                if(users != null) {
                    root.ma_username.visibility = View.VISIBLE
                    root.ma_profile.setOnClickListener {

                    }
                    Picasso.get().load(users.profileImageUrl).into(root.ma_profile)
                    root.ma_username.text = users.username
                }
            }

        })

        return root
    }
}
