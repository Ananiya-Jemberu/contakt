package com.ultraone.contakt.messaging.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.ultraone.contakt.R
import com.ultraone.contakt.data.AllUsers
import com.ultraone.contakt.data.Chat
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.chating_from.view.*
import kotlinx.android.synthetic.main.chating_to.view.*
import kotlinx.android.synthetic.main.image_sending.view.*
import kotlinx.android.synthetic.main.latest_messages.view.*

//for ChattingActivity
class SendFile(val fileUri: Chat, val users: AllUsers?) : Item<ViewHolder>() {
        // hello(): String = "hello"


        override fun getLayout(): Int {
            return R.layout.image_sending


        }


        override fun bind(viewHolder: ViewHolder, position: Int) {
            if (fileUri.imageUri != null && fileUri.imageUri != "") {
                val image_holder = viewHolder.itemView.image_holder
                image_holder.setOnClickListener {

                    viewHolder.itemView
                }

                viewHolder.itemView.iv_to.visibility = View.VISIBLE
                viewHolder.itemView.iv_from.visibility = View.GONE
                viewHolder.itemView.root.visibility = View.VISIBLE
                viewHolder.itemView.root2.visibility = View.GONE
                fileUri.let { Log.d("My error of Picasso", it.imageUri) }
                Picasso
                    .get()
                    .load(fileUri.imageUri)
                    //.transform(CircleTransform())
                    .into(image_holder)

            }
            Picasso.get().load(users?.profileImageUrl).into(viewHolder.itemView.iv_to)
        }
    }

class ReceiveFile(val fileUri: Chat, val users: AllUsers) : Item<ViewHolder>() {
        override fun getLayout(): Int {
            return R.layout.image_sending

        }

        override fun bind(viewHolder: ViewHolder, position: Int) {
            if (fileUri.imageUri != null && fileUri.imageUri != "") {
                val image_holder = viewHolder.itemView.image_holder
                viewHolder.itemView.iv_to.visibility = View.GONE
                viewHolder.itemView.iv_from.visibility = View.VISIBLE
                viewHolder.itemView.root.visibility = View.GONE
                viewHolder.itemView.root2.visibility = View.VISIBLE

                Picasso
                    .get()
                    .load(fileUri.imageUri)
                    //.transform(RoundedTransformation(50, 4))
                    .into(image_holder)
            }
            Picasso.get().load(users.profileImageUrl).into(viewHolder.itemView.iv_from)
        }
    }

class ChattingFrom(val data: Chat?, val users: AllUsers) : Item<ViewHolder>() {
        override fun getLayout(): Int {
            return R.layout.chating_from

        }

        override fun bind(viewHolder: ViewHolder, position: Int) {
            if(data != null) {
                viewHolder.itemView.cv_time1.text = (data.timeSpam.toString())
                viewHolder.itemView.recieve.text = data.text
                Picasso.get().load(users.profileImageUrl).into(viewHolder.itemView.cv_from)
            }
        }

    }

class ChattingTo(val data: Chat?, val users: AllUsers?) : Item<ViewHolder>() {
        override fun getLayout(): Int {
            return R.layout.chating_to

        }

        override fun bind(viewHolder: ViewHolder, position: Int) {
            if(data != null) {
                viewHolder.itemView.cv_time2.text = (data.timeSpam.toString())
                viewHolder.itemView.send.text = data.text
                Picasso.get().load(users?.profileImageUrl).into(viewHolder.itemView.cv_to)
            }
        }

    }
//
//for MessagingActivity
class MessageRows(val chat: Chat?): Item<ViewHolder>(){
    var partnerUser: AllUsers? = null
    override fun getLayout(): Int {
        return R.layout.latest_messages
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {

        val chatPID: String
        if(chat?.fromId == FirebaseAuth.getInstance().uid){
           chatPID = chat!!.toId
        }else{
           chatPID = chat!!.fromId
        }
        /*FirebaseDatabase.getInstance().getReference("/users/${chat.toId}").let {
            it.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }
                override fun onDataChange(p0: DataSnapshot) {
                    partnerUser = p0.getValue(AllUsers::class.java)
                    partnerUser.let {pU ->

                        Picasso.get().load(pU?.profileImageUrl)
                            .into(viewHolder.itemView.circleImageViewOfLM)
                    }
                    viewHolder.itemView.ma_un.let {tv ->
                        val users = partnerUser
                        if (users?.uid == FirebaseAuth.getInstance().uid) {
                            tv.text =  users?.username

                        } else {

                            tv.text = users?.username
                            tv.visibility = View.VISIBLE
                        }
                    }
                }
            })
        }*/
        val ref = FirebaseDatabase.getInstance().getReference("/users/$chatPID")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(p0: DataSnapshot) {
                //val users = p0.getValue(AllUsers::class.java)
                partnerUser = p0.getValue(AllUsers::class.java)
                val users = partnerUser
                partnerUser.let {pU ->

                    Picasso.get().load(pU?.profileImageUrl)
                        .into(viewHolder.itemView.circleImageViewOfLM)
                }
                viewHolder.itemView.ma_un.let {tv ->

                        tv.text =  users?.username

                }
                if(users?.uid == FirebaseAuth.getInstance().uid){
                    viewHolder.itemView.ma_lm.apply {
                        setTextColor(Color.BLACK)
                        setBackgroundResource(R.drawable.text_bg2)
                    }

                }

            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
        Picasso.get()
        viewHolder.itemView.ma_un.text = ""
        viewHolder.itemView.ma_lm.text = chat?.text
    }

}
//