package com.ultraone.contakt.messaging

import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.ultraone.contakt.R
import com.ultraone.contakt.authincation.SignUpActivity
import com.ultraone.contakt.data.AllUsers
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_creating_message.*
import kotlinx.android.synthetic.main.all_users.view.*

class CreatingMessage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creating_message)
        supportActionBar?.title = "Select People"
        getUsers()
    }
    companion object{
        val USER_KEY = "USER_KEY"
    }

    private fun getUsers() {
        val rf = FirebaseDatabase.getInstance().getReference("/users")
        rf.addListenerForSingleValueEvent(object : ValueEventListener {
            val adapter = GroupAdapter<ViewHolder>()
            override fun onCancelled(p0: DatabaseError) {

                SignUpActivity().errorDialog(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    shimmerOfUsers.stopShimmer()
                    shimmerOfUsers.visibility = View.GONE
                    rv.visibility = View.VISIBLE
                    val user = it.getValue(AllUsers::class.java)
                    Log.d("hello", it.toString())
                    if(user != null){
                    adapter.add(Users(user))}
                }

                adapter.setOnItemClickListener{item, view ->
                    val users = item as Users
                    val intent = Intent(view.context, ChattingActivity::class.java)
                    intent.putExtra(USER_KEY,users.users)
                    startActivity(intent)
                    finish()
                }
                rv.adapter = adapter
            }

        })
    }

}
class Users(val users: AllUsers): Item<ViewHolder>(){
    override fun getLayout(): Int {
        return R.layout.all_users

    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
       Picasso.get().load(users.profileImageUrl).into(viewHolder.itemView.imageView2)

        viewHolder.itemView.user_name.text = users.username
    }

}
