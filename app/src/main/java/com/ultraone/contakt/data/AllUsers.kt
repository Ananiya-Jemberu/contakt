package com.ultraone.contakt.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class AllUsers (val username: String,
                val profileImageUrl: String,
                val uid: String): Parcelable{
    constructor() : this("","","")
}