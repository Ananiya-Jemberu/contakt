package com.ultraone.contakt.data

class Chat(
    val id: String, val fromId: String,
    val toId: String,
    val text: String?, val imageUri: String?, val timeSpam: String
){
    constructor() : this("","","","","","")
}